import unittest

from interest_calculation import calc_interest, _interest


class TestInterest(unittest.TestCase):
    def test_interest(self):
        self.assertEqual(_interest(10000, 10000, 36), 368321.68679189606)

    def test_main(self):
        self.assertEqual(calc_interest(100, 100, 36), "R$ 3683.22")


if __name__ == "__main__":
    unittest.main()
