# Interest Calculation

This is a Python 3 project to calculate the final amount of a weekly investment at some interest rate.

The files:

- `settings.py`: Here we define:
    - The annual interest rate percentage
    - The total work days in the year

- `interest_calculation.py`: The file that will make all the calc and return the final amount

- `main.py`: The main file for use by the command line utility

- `test_functions.py`: unit tests of the function of `interest_calculation.py`

## Requirements

The only requirement to run this project is Python 3. All the libs used in the project are available in the standard library. So there's no need to install anything.

**NOTE**: This project is Python 3 only. It'll not work correctly with Python 2.

## How to run this project

1 - Clone the repo with the command `git clone git@bitbucket.org:mmxx/interest.git`

2 - Call the main file passing the args, eg: `./main.py -A 100 -I 100 -W 36` or `python3 main.py -A 100 -I 100 -W 36`

 In case of doubts about the args, use `python3 main.py --help`
