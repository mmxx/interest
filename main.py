#! /usr/bin/env python3

import argparse

from interest_calculation import calc_interest

parser = argparse.ArgumentParser(
    description="Command line utility to calc the final amount of weekly investiments."
)
parser.add_argument(
    "-A",
    "--amount",
    type=float,
    help="The initial amount",
    required="True",
    default="100",
)
parser.add_argument(
    "-I",
    "--investiment",
    type=float,
    help="The weekly investiment",
    required="True",
    default="100",
)
parser.add_argument(
    "-W",
    "--weeks",
    type=int,
    help="The total weeks of investiment",
    required="True",
    default="36",
)

args = parser.parse_args()

if __name__ == "__main__":
    result = calc_interest(args.amount, args.investiment, args.weeks)
    print(result)
